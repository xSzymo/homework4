package com.homework.services;

public interface PurchaseI<E> {
    void purchase(E e);
}
