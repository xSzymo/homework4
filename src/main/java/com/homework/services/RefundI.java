package com.homework.services;

public interface RefundI<E> {
    void refund(E e);
}
