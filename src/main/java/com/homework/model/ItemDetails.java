package com.homework.model;

public class ItemDetails {
    public Item item;
    public long quantity;
    public long shippingCharge;
}
