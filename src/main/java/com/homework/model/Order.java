package com.homework.model;

import java.util.Set;

public class Order {
    public long orderId;
    public Set<OrderPart> orderParts;

    public boolean isCompleted() {
        if(orderParts != null)
            return orderParts.stream().allMatch(order -> order.status.equals(Status.COMPLETED));
        return false;
    }
}
