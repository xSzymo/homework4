package com.homework.model;

public enum Status {
    COMPLETED, INCOMPLETED, ERROR;
}
