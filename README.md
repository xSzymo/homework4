There are 6 models

- Order
- OrderPart
- Payment
- Refund
- Item
- ItemDetails

Order -> OrderPart -> Refund | Payment | Collection of ItemDetails -> Item

I created order which might have multiple order parts.<br>
One order part is single transaction used to one or multiple items identified from collection of item details.<br>
This way it's easy to refund single order part and keep a record of payment for such transaction.<br>

I started work with moving models from description to application model. 
There is not much business logic, I just focused on architecture, but if I have started to add logic, I would try to start with unit tests and TDD approach.  